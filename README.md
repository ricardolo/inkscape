SCADAvis.io Synoptic Display Editor (April/2018)
================================================

Forked from:
[https://gitlab.com/inkscape/](https://gitlab.com/inkscape/) 
and from:
[Inkscape+SAGE](https://sourceforge.net/projects/sage-scada/).

Modified and custom built for use with:
 * [SCADAvis.io](https://scadavis.io)
 * [xPlainCloud.com](https://xplaincloud.com)
 * [OSHMI - Open Substation HMI](https://sourceforge.net/projects/oshmiopensubstationhmi/)

The original SVG editor was extended to be capable of providing SCADA animations markup to SVG objects.
In this way, a Javascript engine can read the markup from the SVG file and animate the graphics in real time with live data.

This fork is a derivative of upstream work, not in any way associated with the original authors.
We respect and intend to comply with the branding policy of the Inkscape Project, https://inkscape.org/en/about/branding/.
In our view, the modifications introduced are not substantial and do not intend to create a competitive product.
The modifications introduced are meant to allow the use of the software as a SCADA Synoptic Editor.
We respect the GPL license of the upstream work by making the source code of modifications to the original Inkscape and SAGE code available here.

Inkscape is an open source drawing tool with capabilities similar to
Illustrator, Freehand, and CorelDraw that uses the W3C standard scalable
vector graphics  format (SVG). Some supported SVG features include
basic shapes, paths, text, markers, clones, alpha blending, transforms,
gradients, and grouping. In addition, Inkscape supports Creative Commons
meta-data, node-editing, layers, complex path operations, text-on-path,
and SVG XML editing. It also imports several formats like EPS, Postscript,
JPEG, PNG, BMP, and TIFF and exports PNG as well as multiple vector-based
formats.

For compilation see:

http://wiki.inkscape.org/wiki/index.php/Compiling_Inkscape_on_Windows_with_MSYS2

[Licensed under the GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

